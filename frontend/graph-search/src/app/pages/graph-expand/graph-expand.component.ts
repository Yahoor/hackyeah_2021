import { Component, OnInit } from '@angular/core';
import {
  AfterViewInit,
  ElementRef,
  HostListener,
  VERSION,
  ViewChild
} from "@angular/core";
import ForceGraph3D, {
  ConfigOptions,
  ForceGraph3DInstance
} from "3d-force-graph";

import * as THREE from 'three'
import { CSS2DObject } from 'three-css2drender';


interface Node {
  id: number;
}

interface Link {
  source: number;
  target: number;
  group: number;
}
@Component({
  selector: 'app-graph-expand',
  templateUrl: './graph-expand.component.html',
  styleUrls: ['./graph-expand.component.scss']
})
export class GraphExpandComponent implements AfterViewInit {

  private graph!: ForceGraph3DInstance;

  rootId = 0;

  // Random tree
  N = 300;
  gData = {
    nodes: [...Array(this.N).keys()].map(i => ({ id: i, collapsed: i !== this.rootId, childLinks: [] })),
    links: [...Array(this.N).keys()]
      .filter(id => id)
      .map(id => ({
        source: Math.round(Math.random() * (id - 1)),
        target: id
      }))
  };

  constructor(private elementRef: ElementRef) { }

  ngAfterViewInit(): void {

    const nodesById = Object.fromEntries(this.gData.nodes.map((node: any) => [node.id, node]));
    this.gData.links.forEach(link => {
      nodesById[link.source].childLinks.push(link);
    });

    let that = this;

    const getPrunedTree = () => {
      const visibleNodes = [];
      const visibleLinks = [];
      (function traverseTree(node = nodesById[that.rootId]) {
        visibleNodes.push(node);
        if (node.collapsed) return;
        visibleLinks.push(...node.childLinks);
        node.childLinks
          .map((link: any) => ((typeof link.target) === 'object') ? link.target : nodesById[link.target]) // get child node
          .forEach(traverseTree);
      })(); // IIFE

      return { nodes: visibleNodes, links: visibleLinks };
    };

    this.graph = ForceGraph3D()(this.htmlElement)
      .graphData(getPrunedTree())
      .linkDirectionalParticles(2)
      .nodeColor((node: any) => !node.childLinks.length ? 'green' : node.collapsed ? 'red' : 'yellow')
      //.onNodeHover((node: any) => this.htmlElement.style.cursor = node && node.childLinks.length ? 'pointer' : null)
      .onNodeClick((node: any) => {
        if (node.childLinks.length) {
          node.collapsed = !node.collapsed; // toggle collapse state
          this.graph.graphData(getPrunedTree());
        }

      })
    }

  @HostListener("window:resize")
  public windowResize(): void {
    const box = this.htmlElement.getBoundingClientRect();
    this.graph?.width(box.width);
    this.graph?.height(box.height);
    // @ts-ignore
    this.graph?.controls().handleResize();
  }

  private get htmlElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }

}
