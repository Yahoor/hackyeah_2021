import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphExpandComponent } from './graph-expand.component';

describe('GraphExpandComponent', () => {
  let component: GraphExpandComponent;
  let fixture: ComponentFixture<GraphExpandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphExpandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphExpandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
