import { Component, OnInit } from '@angular/core';
import {
  AfterViewInit,
  ElementRef,
  HostListener,
  VERSION,
  ViewChild
} from "@angular/core";
import ForceGraph3D, {
  ConfigOptions,
  ForceGraph3DInstance
} from "3d-force-graph";

import * as THREE from 'three'
import { CSS2DObject }  from 'three-css2drender';


interface Node {
  id: number;
}

interface Link {
  source: number;
  target: number;
  group: number;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {

  private graph!: ForceGraph3DInstance;

  

  constructor(private elementRef: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.graph = ForceGraph3D()(this.htmlElement)
      .jsonUrl('./assets/data.json')
      /*.nodeThreeObject((node: any) => {
        const nodeEl = document.createElement('div');
        nodeEl.textContent = node.id;
        nodeEl.style.color = node.color;
        nodeEl.className = 'node-label';

        return new CSS2DObject(nodeEl);
      })*/
      .nodeLabel('id')
      .nodeAutoColorBy('group')
      .onNodeClick((node: any) => {
        const distance = 40;
        const distRatio = 1 + distance / Math.hypot(node.x, node.y, node.z);

        this.graph.cameraPosition(
          { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
          node, // lookAt ({ x, y, z })
          3000  // ms transition duration
        );
      })

    /*this.graph.linkColor(( group ) => group ? '#ffffff' : '#ff9c91');
    this.graph.linkWidth(( group ) => group ? 2 : 10);
    this.graph.d3Force("link")!.distance( (group: any) => (group ? 20 : 100));
    this.graph.graphData(this.gData);*/

    this.windowResize();
  }

  @HostListener("window:resize")
  public windowResize(): void {
    const box = this.htmlElement.getBoundingClientRect();
    this.graph?.width(box.width);
    this.graph?.height(box.height);
    // @ts-ignore
    this.graph?.controls().handleResize();
  }

  private get htmlElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }
}
