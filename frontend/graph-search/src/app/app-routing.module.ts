import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraphExpandComponent } from './pages/graph-expand/graph-expand.component';
import { HomeComponent } from './pages/home/home.component';
import { SearchComponent } from './pages/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent
  },
  {
    path: 'graph',
    component: HomeComponent
  },
  {
    path: 'graph2',
    component: GraphExpandComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
