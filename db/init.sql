CREATE TABLE public.objects
(
    id serial NOT NULL,
    category text NOT NULL,
    parent_site text NOT NULL,
    textual_content text NOT NULL,
    url text NOT NULL,
    date text NOT NULL,
    tags text NOT NULL,
    vector text NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.objects
    OWNER to postgres;

CREATE INDEX cat_par_hash
    ON public.objects USING btree
    (category ASC NULLS LAST, parent_site ASC NULLS LAST, url_hash ASC NULLS LAST)
;