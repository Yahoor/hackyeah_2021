import io
import re
import csv
import fitz
import docx
import hashlib
import requests
import numpy as np
from PIL import Image
from PIL.ExifTags import TAGS
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from dataclasses import dataclass
from typing import Optional, List, Dict, Any, Set, Union
from ..tagger.tagger import get_tags, tags2vec_str

re_all_headers = re.compile('^h[1-6]$')
re_all_pds = re.compile(r'(.pdf)')
re_all_docs = re.compile(r'(.docx)')
re_all_tags = re.compile(r'<[^>]+>')


def style_background_image_url(style: str) -> dict:
    for kv in style.split(";"):
        k, v = kv.split(":")
        if k == "background-image":
            return v.replace(" ", "").replace("\'", "\"").lstrip("url(\"").rstrip("\")")


def fold_link(base_site: str, partial_link: str) -> str:
    if not partial_link.startswith("http"):
        return urljoin(base_site, partial_link)
    return partial_link


def text_cleaner(text: str) -> str:
    return re_all_tags.sub(' ', " ".join(text.split()))


def retrive_parent_url(urls: List[str], url):
    for u in urls:
        if u in url:
            return u
    return url


def contain_blacklist(text: str, blacklist: List[str]) -> bool:
    for b in blacklist:
        if b in text:
            return False
    return True


def get_image_data(url: str) -> str:
    response = requests.get(url)
    img = Image.open(io.BytesIO(response.content))
    exifdata = img.getexif()
    img = None
    for tagid in exifdata:
        if "date" in TAGS.get(tagid, tagid).lower():
            return exifdata.get(tagid)
    return ""


def pdf2txt(url: str) -> Union[str, fitz.Document]:
    response = requests.get(url)
    with fitz.open(stream=io.BytesIO(response.content), filetype="pdf") as pdfReader:
        return " ".join(page.get_text() for page in pdfReader.pages()), str(pdfReader.metadata["modDate"])


def docx2txt(url: str) -> Union[str, str]:
    response = requests.get(url)
    docxReader = docx.Document(io.BytesIO(response.content))
    return " ".join(paragraph.text for paragraph in docxReader.paragraphs), docxReader


# crawlers

class RepChecker:

    def __init__(self):
        self.reps = set()

    def is_in(self, url: str) -> bool:
        h_url = hashlib.md5(url.encode())
        return h_url in self.reps

    def add(self, url: str):
        self.reps.add(hashlib.md5(url.encode()))


@dataclass
class SearchEntity:
    """ Scrapping data entity model
    """

    category: str  # img, text, pdf, docx/doc, video, header
    parent_site: str  # parent site adress

    # category specific
    textual_content: Optional[str] # text, pdf, docx
    url: Optional[str] # pdf, docx, video, img
    date: str
    tags: Optional[List[str]]
    vector: np.array

    def save_to_csv(self, csv_writer, rep_checker):
        # ["category", "parent_site", "textual_content", "url", "date" "tags", "vector"]
        textual_content = text_cleaner(self.textual_content)
        if not rep_checker.is_in(self.url) and len(textual_content) > 0 and len(self.url) > 0:
            return csv_writer.writerow(
                [self.category, self.parent_site, textual_content, self.url, self.date, self.tags, self.vector]
            )


def search_for_images(bs_obj: BeautifulSoup, parent_site: str, parent_extended: str, csv_writer, rep_checker):
    for img in bs_obj.find_all("img"):
        try:
            if "alt" in img.attrs and len(img.attrs["alt"]) > 0:
                text = img.attrs["alt"] # description
                tags = get_tags(text)
                vec = tags2vec_str(tags)
                url = fold_link(parent_site, img.attrs["src"])
                SearchEntity(
                    category="img",
                    parent_site=parent_site,
                    textual_content=text,
                    url=url,
                    date=get_image_data(url),
                    tags=str(tags),
                    vector=vec
                ).save_to_csv(csv_writer, rep_checker)
        except:
            continue
    for img in bs_obj.find_all(attrs={"style": True}):
        try:
            style = img.get("style")
            if "background-image" in style and ":url" in style.replace(" ", ""):
                text = " ".join([" ".join(h.contents) for h in img.parent.find_all(re_all_headers)]) # all parent headers
                if len(text) > 0:
                    tags = get_tags(text)
                    vec = tags2vec_str(tags)
                    url = fold_link(parent_site, style_background_image_url(style))
                    SearchEntity(
                        category="img",
                        parent_site=parent_site,
                        textual_content= text,
                        url=url,
                        date=get_image_data(url),
                        tags=str(tags),
                        vector=vec
                    ).save_to_csv(csv_writer, rep_checker)
        except:
            continue


def search_for_text(bs_obj: BeautifulSoup, parent_site: str, parent_extended: str, csv_writer, rep_checker):
    blacklist = {
        '[document]',
        'noscript',
        'header',
        'html',
        'meta',
        'head', 
        'input',
        'script'
    }
    text = ""
    for tag in bs_obj.find_all(text=True):
        if tag.parent.name not in blacklist:
            text += f"{tag} "
    tags = get_tags(text)
    vec = tags2vec_str(tags)
    SearchEntity(
        category="text",
        parent_site=parent_site,
        textual_content=text,
        url=parent_extended,
        date="",
        tags=str(tags),
        vector=vec
    ).save_to_csv(csv_writer, rep_checker)


def search_for_headers(bs_obj: BeautifulSoup, parent_site: str, parent_extended: str, csv_writer, rep_checker):
    text = ""
    for h in bs_obj.find_all(re_all_headers):
        text += f"{h.text} "
    tags = get_tags(text)
    vec = tags2vec_str(tags)
    SearchEntity(
        category="header",
        parent_site=parent_site,
        textual_content=text,
        url=parent_site,
        date="",
        tags=str(tags),
        vector=vec
    ).save_to_csv(csv_writer, rep_checker)


def search_for_pdfs(bs_obj: BeautifulSoup, parent_site: str, parent_extended: str, csv_writer, rep_checker):
    for pdf in bs_obj.find_all(href=re_all_pds):
        try:
            url = fold_link(parent_site, pdf.attrs["href"])
            text, date = pdf2txt(url)
            tags = get_tags(text)
            vec = tags2vec_str(tags)
            SearchEntity(
                category="pdf",
                parent_site=parent_site,
                textual_content=text,
                url=url,
                date=date,
                tags=str(tags),
                vector=vec
            ).save_to_csv(csv_writer, rep_checker)
        except:
            continue


def search_for_docs(bs_obj: BeautifulSoup, parent_site: str, parent_extended: str, csv_writer, rep_checker):
    for pdf in bs_obj.find_all(href=re_all_docs):
        try:
            url = fold_link(parent_site, pdf.attrs["href"])
            text, date = docx2txt(url)
            tags = get_tags(text)
            vec = tags2vec_str(tags)
            SearchEntity(
                category="docx",
                parent_site=parent_site,
                textual_content=text,
                url=url,
                date=date,
                tags=str(tags),
                vector=vec,
            ).save_to_csv(csv_writer, rep_checker)
        except:
            continue


def general_crawler(urls: List[str], csv_path: str):
    
    url_to_visit = set(urls)
    rep_checker = RepChecker()

    with open(csv_path, "a") as csv_file:

        csv_writer = csv.writer(csv_file, delimiter='\t')
        csv_writer.writerow(["category", "parent_site", "textual_content", "url", "date", "tags", "vector"])

        while len(url_to_visit) > 0:
            
            url = url_to_visit.pop()
            parent_url = retrive_parent_url(urls, url)
            try:
                response = requests.get(url)
            except:
                continue
            bs_obj = BeautifulSoup(response.content, "html.parser")

            # update urls
            links = [fold_link(url, link.attrs["href"]) for link in  bs_obj.find_all("a", href=True) \
                if not "http" in str(link.attrs["href"]) and not str(link.attrs["href"]).replace(" ", "").endswith(("gif", "pdf", "doc", "docx", "png", "jpg", "jpeg"))]
            
            for sub_url in links:
                if not rep_checker.is_in(sub_url):
                    url_to_visit.add(sub_url)
            
            print("url:", url)
            print("parent:", parent_url)
            print("url_to_visit:", len(url_to_visit) )

            # extract entities
            search_for_pdfs(bs_obj, parent_url, url, csv_writer, rep_checker)
            search_for_text(bs_obj, parent_url, url, csv_writer, rep_checker)
            search_for_docs(bs_obj, parent_url, url, csv_writer, rep_checker)
            search_for_images(bs_obj, parent_url, url, csv_writer, rep_checker)
            # search_for_headers(bs_obj, url, csv_writer, rep_checker)


def get_video_items_batch(playlist_id: str, api_key: str, max_results, token=""):
    r = requests.get(url=f"https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlist_id}&key={api_key}&part=snippet&maxResults={max_results}&pageToken={token}")
    return r.json()


def you_tube_crawler(playlist_id: str, api_key: str, csv_path: str, max_results: int=50):
    with open(csv_path, "a") as csv_file:

        csv_writer = csv.writer(csv_file, delimiter='\t')
        csv_writer.writerow(["category", "parent_site", "textual_content", "url", "date" "tags", "vector"])
        rep_checker = RepChecker()

        pageToken = ""
        while(pageToken != None):
            response = get_video_items_batch(playlist_id, api_key, max_results, pageToken)
            items = response['items']
            pageToken = response.get('nextPageToken', None)

            for item in items:
                snippet = item['snippet']
                text = f"{snippet['title']} {snippet['description']}"
                tags = get_tags(text)
                vec = tags2vec_str(tags)
                video_id=snippet['resourceId']['videoId']
                SearchEntity(
                    category="video",
                    parent_site="https://www.youtube.com/",
                    textual_content=text,
                    url=f"https://www.youtube.com/watch?v={video_id}",
                    date=snippet['publishedAt'],
                    tags=str(tags),
                    vector=vec
                ).save_to_csv(csv_writer, rep_checker)

