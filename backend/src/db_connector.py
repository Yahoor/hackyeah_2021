import psycopg2
from enum import Enum
from typing import Any


db_name="HackYeah"
db_user="postgres"
db_password="postgres"
db_host="0.0.0.0"
db_port="5432"


class DBFetch(Enum):
    NONE = 0
    ONE = 1
    ALL = 2


class DBConnector:
    
    _connection: Any

    def __init__(self):
        self._connection = None
        self._connect()

    def query(
                self,
                query_str: str,
                fetch: DBFetch = DBFetch.NONE
            ) -> Any:
        """ Fetch data from database, handle connection lost.
        """
        data = None
        connection_error = True
        retries = 0
        while retries < 3 and connection_error:
            retries += 1
            try:
                with self._connection:
                    with self._connection.cursor() as curs:
                        curs.execute(query_str)
                        if fetch == DBFetch.ONE: 
                            data = curs.fetchone()
                        elif fetch == DBFetch.ALL: 
                            data = curs.fetchall()
                connection_error = None
            except psycopg2.InterfaceError as e:
                print( "Database connection lost, reconnecting...")
                connection_error = e
                self._connect()
        if connection_error:
            raise connection_error
        return data

    def _connect(self):
        if self._connection:
            self._connection.close()
        self._connection = psycopg2.connect(
            
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
            connect_timeout=5,
            keepalives=1,
            keepalives_idle=60,
            keepalives_interval=10,
            keepalives_count=5
        )
        self._connection.autocommit = True

    def __del__(self):
        if self._connection:
            self._connection.close()