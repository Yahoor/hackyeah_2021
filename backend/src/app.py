from typing import Union, Dict
import fastapi
import concurrent
import numpy as np
from db_connector import DBConnector, DBFetch


# UTILS

def load_obj_vecs() -> Union[np.array, Dict]:
    rows = DBConnector(
        "select from url_hash, vector from public.objects;",
        DBFetch.ALL
    )
    vecs_l = []
    vecs_hash_mapping = dict()
    for idx, row in enumerate(rows):
        pass




# VARS

executor = concurrent.futures.ProcessPoolExecutor(
    max_workers=4,
)


app = fastapi.FastAPI("Gate of Memory")



# ROUTES

@app.get("/search")
def search():
    pass
