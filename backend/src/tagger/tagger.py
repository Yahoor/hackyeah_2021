import os
import re
import spacy
import collections
import numpy as np
from typing import List, Tuple


wordnet_dict_path = os.path.join(os.path.dirname(__file__), "dict-pl-5.txt")

nlp = spacy.load("pl_core_news_md")

words_blacklist = set.union(nlp.Defaults.stop_words, {"-","%",":","|","'","\\","↑","}","{","[","]"})
pos_blacklist = {"ADV","DET","SYM","CCONJ","PUNCT","AUX","SCONJ","X","ADP","PART","SPACE","NUM","INTJ","PRON"}

with open(wordnet_dict_path, "r") as wordnet_file:
    wordnet_lines = wordnet_file.readlines()[1:]
    wordnet_lemmas = {fl.split('\t')[0] for fl in wordnet_lines}
    wordnet_mapping = {fl.split('\t')[0]: fl.split('\t')[1] for fl in wordnet_lines}


def get_most_common(texts: List[Tuple[str, int]], treshold: float) -> List[str]:
    t_ctr = collections.Counter(texts).most_common()
    t_len = len(texts)
    r_len, res = 0, []
    for t in t_ctr:
        if r_len < treshold * t_len:
            res.append(t[0])
            r_len += t[1]
        else:
            break
    return res


def tags2vec(tags: List[str]) -> np.array:
    return nlp(" ".join(t for t in tags)).vector


def tags2vec_str(tags: List[str]) -> str:
    return str(tags2vec(tags).tolist())
    

def chunkstring(string, length):
    return (string[0+i:length+i] for i in range(0, len(string), length))


def get_tags(
            textt: str, 
            mc_treshold: float = 0.3,
            ne_treshold: float = 0.5,
            as_list: bool = True
        ) -> List[str]:

    textt = re.sub("[\t\n ]+", " ", textt)[:5000000]
    res = []
    for text in chunkstring(textt, 600000):
        doc = nlp(text)
        most_common = []
        named_entities = []
        ngrams_wordnet = []
        lemmas = []
        
        # most common tags
        for token in doc:
            if token.pos_ not in pos_blacklist \
                    and token.text not in words_blacklist \
                    and len(token.text) > 1:
                lemmas.append(token.lemma_)  
        most_common = get_most_common(lemmas, mc_treshold) # treshold=0.3
        
        # ner tags
        named_entities = [ne.text for ne in doc.ents]
        named_entities = get_most_common(named_entities, ne_treshold) # treshold=0.3
        
        # ngram tags
        bigrams = set(f"{x} {y}" for x, y in zip(lemmas[0:], lemmas[1:]))
        trigrams = set(f"{x} {y} {z}" for x, y, z in zip(lemmas[0:], lemmas[1:], lemmas[2:]))

        bigrams = bigrams.intersection(wordnet_lemmas)
        trigrams = trigrams.intersection(wordnet_lemmas)
        ngrams_wordnet = bigrams.union(trigrams)  
        
        ngrams_wordnet = {wordnet_mapping[ngw].replace("\n", "") for ngw in ngrams_wordnet} 
        
        res += most_common
        res += named_entities
        res += list(ngrams_wordnet)

    res = set(res)
    if len(res) > 0:
        return res
    return textt.split()
