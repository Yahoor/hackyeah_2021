import csv
import hashlib
from src.db_connector import DBConnector, DBFetch

csv_path = "gen_crw.csv"
db = DBConnector()

with open(csv_path, "r") as csv_file:

    csv_reader = csv.reader(csv_file, delimiter='\t')

    headers = next(csv_reader, None) # "category", "parent_site", "textual_content", "url", "date", "tags", "vector"

    for row in csv_reader:
        print(len(row))
        textual_content = row[2].replace("\'", "\"")
        date = row[4] if len(row[4]) > 0 else "-"
        tags = row[5].replace("\'", "\"")
        db.query(
            f"INSERT INTO public.objects (category, parent_site, textual_content, url, date, tags, vector, url_hash) values (\'{row[0]}\',\'{row[1]}\',\'{textual_content}\',\'{row[3]}\',\'{date}\',\'{tags}\',\'{row[6]}\',\'{hashlib.md5(row[3].encode())}\')",
            fetch=DBFetch.NONE
        )
